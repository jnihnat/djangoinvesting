FROM python:3.9-slim-buster
ARG REQUIREMENTS_FILE=requirements_dev

COPY app /var/www/app
WORKDIR /var/www/app


RUN apt-get update && apt-get install build-essential bash -y

# TA-Lib
RUN cd ./Resources && \
    tar -xvf ta-lib-0.4.0-src.tar.gz && \
    cd ./ta-lib && \
    ./configure --prefix=/usr && \
    make && \
    make install && \
    cd .. && \
    rm -R ta-lib

RUN python -m pip install --upgrade pip
RUN pip install -r ${REQUIREMENTS_FILE}
RUN apt-get update

RUN mkdir -p /vol/web/static
RUN chmod -R 755 /vol/web

CMD ["./startup.sh"]
