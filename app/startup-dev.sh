#!/bin/bash


RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color

# Give some time to kick off the postgres.
sleep 3

# Run migrations.

if python ./manage.py migrate; then
    echo -e "${GREEN}Database migrated.${NC}"
else
    echo -e "${RED}Couldn't migrate the database.${NC}"
    exit 1
fi

#if python ./manage.py loaddata db.json; then
#    echo -e "${GREEN}Fixtures loaded.${NC}"
#else
#    echo -e "${RED}Couldn't load fixtures.${NC}"
#    exit 1
#fi

# python3 manage.py collectstatic --noinput
# Run the app.
celery -A investing beat -l info -S django --pidfile= &

celery -A investing worker -l info &

gunicorn -c gunicorn.py investing.wsgi:application