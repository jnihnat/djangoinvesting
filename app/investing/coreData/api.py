from datetime import datetime
import requests
from django.conf import settings
from abc import ABC, abstractmethod
import pandas as pd
import logging

logger = logging.getLogger(__name__)

class HistoricalDataAbstractClass(ABC):

    def call_api(self):
        return requests.get(self.url,params=self.payload)
    @abstractmethod
    def return_structured_data(self):
        pass


class TingoHistoricalData(HistoricalDataAbstractClass):

    def __init__(self, start_date="2010-01-01", columns="open,high,low,close,volume,adjClose") -> None:
        self.payload = {
        "startDate": start_date,
        # "endDate": "2022-01-26",
        "token": settings.TIINGO_API_KEY,
        "columns":columns,
    }
            
        

    def return_structured_data(self, stock_ticker):
        self.url = f"https://api.tiingo.com/tiingo/daily/{stock_ticker}/prices"
        self.response = self.call_api()
        data = pd.json_normalize(self.response.json())
        data = data.set_index("date")
        data = data.rename(columns={"adjClose":"adjusted_close"})
        data.index = pd.to_datetime(data.index)
        data.index = data.index.strftime("%Y-%m-%d")
        #logger.debug(data.index.dtype)
        #logger.debug(data.tail())
        return data.to_json(orient="index")


def get_nasdaq_constituents():
    """
    Calls external service and returns json with nasdaq constituents for today
    """
    HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
        "Referer": "https://www.nasdaq.com",
        "accept-language":"en-US,en;q=0.9",
        "accept-encoding": "gzip, deflate, br",
        "accept": "application/json, text/plain, */*"
    }
    response = requests.get(
        "https://api.nasdaq.com/api/quote/list-type/nasdaq100",
        timeout=10,
        headers=HEADERS,
    )

    new_dict = {"tickers": []}
    for item in response.json()["data"]["data"]["rows"]:
        new_dict["tickers"].append(item["symbol"])
    
    return new_dict

def get_nasdaq_constituents_v2(date=None):
    if not date:
        date = datetime.today().strftime("%Y-%m-%d")
    response = requests.post("https://indexes.nasdaqomx.com/Index/WeightingData",params=f"id=NDX&tradeDate={date}&timeOfDay=SOD")

    new_dict = {"tickers": []}
    for item in response.json()["aaData"]:
        new_dict["tickers"].append(item["Symbol"])

    return new_dict


def get_historical_stock_data(stockTicker):
    """
    Calls external service and returns json with historical stock data for given ticker
    """
    payload = {
        "function": "TIME_SERIES_DAILY_ADJUSTED",
        "symbol": stockTicker,
        "outputsize": "full",
        "apikey": settings.AV_API_KEY,
    }
    return requests.get("https://www.alphavantage.co/query", params=payload)


def get_stock_info(stockTicker):
    """
    Calls external service and returns json with historical stock data for given ticker
    """
    payload = {
        "function": "OVERVIEW",
        "symbol": stockTicker,
        "apikey": settings.AV_API_KEY,
        "outputsize": "full",
    }
    return requests.get("https://www.alphavantage.co/query", params=payload)

def get_stock_info_tiingo(stockTicker):
    """
    Calls external service and returns json with historical stock data for given ticker
    """
    payload = {
        "tickers": stockTicker,
        "token": settings.TIINGO_API_KEY,
    }
    url = "https://api.tiingo.com/tiingo/fundamentals/meta"
    response = requests.get(url, params=payload)
    if response:
        return response.json()[0]["name"]
    else:
        return False

def get_historical_stock_data_tiingo(stockTicker):
    """
    Calls external service and returns json with historical stock data for given ticker
    """
    payload = {
        "startDate": "2010-01-01",
        # "endDate": "2022-01-26",
        "token": settings.TIINGO_API_KEY,
        "columns":"[open,high,low,close,volume,adjClose]",
    }

    url = f"https://www.alphavantage.co/{stockTicker}/prices"
    
    return requests.get(url, params=payload)    
