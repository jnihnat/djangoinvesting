from django.db import models
from django.utils.translation import gettext as _


class Stock(models.Model):
    """
    Model for stocks info
    """

    name = models.CharField(_("Name"), max_length=300)
    ticker = models.CharField(_("Ticker"), max_length=20)

    def __str__(self):
        return self.name


class StockHistoricalData(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    data = models.JSONField(null=True)

    def __str__(self):
        return self.stock.name


class IndicesConstituent(models.Model):
    indice_name = models.CharField(_("Indice Name"), max_length=200)
    indice_ticker = models.CharField(_("Indice Ticker"), max_length=20)
    data = models.JSONField(null=True, blank=True)

    def __str__(self):
        return self.indice_name


class MarketHoliday(models.Model):
    stock_exchange = models.CharField(_("Stock Exchange"), max_length=100)
    holiday_name = models.CharField(_("Holiday Name"), max_length=200)
    date = models.DateField(_("Date"))

    def __str__(self):
        return self.stock_exchange + " " + self.holiday_name
