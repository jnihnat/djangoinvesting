from django.apps import AppConfig


class CoredataConfig(AppConfig):
    name = "coreData"
