import datetime as dt
import pytz
from .customExceptions import NotBusinessDayError, NotAfterHoursTime
from .models import MarketHoliday


def last_bus_day(actual_date=dt.datetime.now(pytz.timezone("America/New_York"))):
    """
    Method for returning last business day after hours
    """
    day_check = False
    while not day_check:
        if actual_date.weekday() in (5, 6):
            bus_day = actual_date - dt.timedelta(days=abs(4 - actual_date.weekday()))
        elif (
            actual_date.weekday()
            in (
                0,
                1,
                2,
                3,
                4,
            )
            and actual_date.time() > dt.time.fromisoformat("20:00")
        ):
            bus_day = actual_date
        elif (
            actual_date.weekday()
            in (
                1,
                2,
                3,
                4,
            )
            and actual_date.time() < dt.time.fromisoformat("20:00")
        ):
            buss_day = actual_date - dt.timedelta(days=1)
        else:
            buss_day = actual_date - dt.timedelta(days=3)
        if MarketHoliday.objects.filter(date=buss_day).exists():
            buss_day = actual_date - dt.timedelta(days=1)
        else:
            day_check = True
    return buss_day


def BussDayCheck(dateToCheck):
    """
    Method to check if provided day is a business day
    """
    if dateToCheck.weekday() in (5, 6):
        raise NotBusinessDayError(date=dateToCheck)
    elif MarketHoliday.objects.filter(date=dateToCheck).exists():
        raise NotBusinessDayError(dateToCheck)
    return


def AfterHoursCheck(timeToCheck):
    """
    Method to check if provided time is not before market close
    """
    if timeToCheck < dt.time(hour=22, minute=30):
        raise NotAfterHoursTime(timeToCheck)
    return