import datetime as dt
import pytz

import pandas as pd
import json
from django.conf import settings
import logging

from .models import IndicesConstituent, StockHistoricalData, Stock
from .helpers import BussDayCheck, AfterHoursCheck
from .customExceptions import NotAfterHoursTime, NotBusinessDayError
from .api import get_nasdaq_constituents_v2, TingoHistoricalData, get_stock_info_tiingo

logger = logging.getLogger(__name__)


def fetch_historical_stock_data(date = None,*args, **kwargs):
    """
    Fetches historical stock data for given stock
    """
    StockBunch = ["NDX100", "SPX500"]
    tickers = []

    today = dt.datetime.now(pytz.timezone("America/New_York"))
    
    now = today.time()
    """
    try:
        BussDayCheck(dateToCheck=today)
    except NotBusinessDayError:
        raise NotBusinessDayError(today)
    
    try:
        AfterHoursCheck(now)
    except NotAfterHoursTime:
        raise NotAfterHoursTime(now)
    """

    for stock in args:
        if stock in StockBunch:
            if date == None:
                use_date = max(
                    IndicesConstituent.objects.filter(
                        indice_ticker=stock
                    ).values_list("data", flat=True)[0].keys()
                )
            else:
                use_date = date
            today_constituents = IndicesConstituent.objects.filter(
                indice_ticker=stock
            ).values_list("data", flat=True)[0][use_date]
            tickers.extend(today_constituents["tickers"])
        else:
            tickers.append(stock)
    api = TingoHistoricalData()
    for stock_ticker in tickers:
        try:
            stock_object = Stock.objects.get(ticker=stock_ticker)
            response_name = True
        except Stock.DoesNotExist:
            response_name = get_stock_info_tiingo(stock_ticker)
            if response_name:
                stock_object = Stock.objects.create(
                    name=response_name, ticker=stock_ticker
                )
        finally:
            if response_name:
                response = api.return_structured_data(stock_ticker)
                StockHistoricalData.objects.update_or_create(
                    stock=stock_object,
                    defaults={
                        "stock": stock_object,
                        "data": json.loads(response),
                    },
                )


def fetch_nasdaq_constituents():
    """
    Fetches today's constituents of the indices for NDX100
    """
    new_dict = get_nasdaq_constituents_v2()

    today = dt.date.today().strftime("%Y-%m-%d")
    
    indice = IndicesConstituent.objects.filter(indice_ticker="NDX100")
    old_data = indice.values("data")
    if old_data[0]["data"]:
        old_data_2 = old_data[0]["data"]
        old_data_2[today] = new_dict
    else:
        old_data_2 = dict()
        old_data_2[today] = new_dict
    indice.update(data=old_data_2)


# fetch_historical_stock_data("AAPL")
# requirements - requests, django, celery, django_celery_results, django_celery_beat, pandas
