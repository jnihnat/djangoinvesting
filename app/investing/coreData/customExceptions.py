class NotBusinessDayError(BaseException):
    """Exception raised when date is not a business day.

    Attributes:
        date -- date which was checked
        message -- explanation of the error
    """

    def __init__(self, date, message="Date is not a business day"):
        self.date = date
        self.message = message
        super().__init__(self, self.date, self.message)

    def __str__(self):
        return f"NotBusinessDayError {self.date} -> {self.message}"


class NotAfterHoursTime(Exception):
    """Exception raised when time is before market close.

    Attributes:
        time -- time which was checked
        message -- explanation of the error
    """

    def __init__(self, time, message="Time is before market close"):
        self.time = time
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"{self.time} -> {self.message}"

class DateNotInData(Exception):
    """Exception raised when requested date is not in data.

    Attributes:
        date -- time which was checked
        message -- explanation of the error
    """

    def __init__(self, date, message="Date is not in stock market data"):
        self.date = date
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"{self.date} -> {self.message}"
