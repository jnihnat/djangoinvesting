from django.shortcuts import render
from django.views.generic import RedirectView
from .scraper import fetch_historical_stock_data
from django.contrib import messages
from django.urls import reverse
from django.utils.translation import gettext as _


class FetchDataView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse("admin:index")