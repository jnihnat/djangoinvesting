from django.contrib import admin
from .models import Stock, StockHistoricalData, IndicesConstituent, MarketHoliday


admin.site.register(StockHistoricalData)
admin.site.register(Stock)
admin.site.register(IndicesConstituent)
admin.site.register(MarketHoliday)