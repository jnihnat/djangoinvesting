from celery import shared_task, Celery
from celery.exceptions import Ignore
from celery.signals import setup_logging
from celery import states
from .scraper import fetch_historical_stock_data, fetch_nasdaq_constituents
from .customExceptions import NotAfterHoursTime, NotBusinessDayError

from traceback import format_exception


app = Celery("investing")

app.config_from_object("django.conf:settings", namespace="CELERY")


@setup_logging.connect
def config_loggers(*args, **kwargs):
    from logging.config import dictConfig  # noqa
    from django.conf import settings  # noqa

    dictConfig(settings.LOGGING)


@app.task(bind=True)
def fetch_stocksData(self, *args, **kwargs):
    date = kwargs.get("date")
    try:
        fetch_historical_stock_data(date, *args)
    except NotBusinessDayError as ex:
        self.update_state(
            state=states.FAILURE,
            traceback=format_exception(type(ex).__name__, ex, ex.__traceback__),
            meta={
                "exc_type": type(ex).__name__,
                "exc_message": ex.message,
                "exc_date": ex.date,
            },
        )
        raise Ignore()
    except NotAfterHoursTime as ex:
        self.update_state(
            state=states.FAILURE,
            traceback=format_exception(type(ex).__name__, ex, ex.__traceback__),
            meta={
                "exc_type": type(ex).__name__,
                "exc_message": ex.message,
                "exc_time": ex.time,
            },
        )
        raise Ignore()


@shared_task
def fetch_constituents():
    fetch_nasdaq_constituents()