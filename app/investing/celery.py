from __future__ import absolute_import

import os

from celery import Celery
from django.conf import settings

# Set the default Django settings module for the 'celery' program.
if "true" == os.environ.get("DJANGO_DEBUG"):
    settings_module = "investing.settings.dev"
else:
    settings_module = "investing.settings.prod"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

app = Celery("investing")

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f"Request: {self.request!r}")

