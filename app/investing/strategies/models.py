from django.db import models
from django.utils.translation import gettext as _


class StrategyType(models.Model):
    """
    Model for strategy types
    """

    name = models.CharField(_("Name"), max_length=300)
    id = models.IntegerField(_("Id"), primary_key=True)


class Strategy(models.Model):
    """
    Model for strategy
    """

    name = models.CharField(_("Name"), max_length=300)
    setup = models.JSONField(_("setup"))
    type = models.ForeignKey(
        StrategyType, verbose_name=_("Strategy type"), on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class StrategyCalculationResult(models.Model):
    """
    Model for stocks info
    """

    strategy = models.ForeignKey(Strategy, on_delete=models.CASCADE)
    date = models.DateField(_("Date"))
    kontext_filter_result = models.BooleanField(_("Kontext filter result"))
    filter_result = models.JSONField(_("Filter result"))

    def __str__(self):
        return self.strategy.name + " " + str(self.date)
