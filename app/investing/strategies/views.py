from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
import logging

import pandas as pd
import json

from .models import StrategyCalculationResult, Strategy
from .forms import SearchStrategyCalculationForm
from .tables import StrategyTable


logger = logging.getLogger(__name__)

# Create your views here.
@login_required
@require_http_methods(["GET"])
def index(request):

    return render(request, "strategies/index.html")

@login_required
@require_http_methods(["GET", "POST"])
def nasdaq_monthly(request, pk):
    table_django = {}
    if request.method == "POST":
        form = SearchStrategyCalculationForm(request.POST)
        if form.is_valid():
            logger.debug("Form is valid")
            request_date = form.cleaned_data["search_date"]
            money_per_company = (
                form.cleaned_data["money_investest"]
                * form.cleaned_data["leverage"]
                / form.cleaned_data["companies_to_buy"]
            )
            table = StrategyCalculationResult.objects.get(
                strategy=pk, date=request_date
            ).filter_result
            #table_json = json.loads(table)
            df = pd.DataFrame.from_dict(
                    table, orient="index", dtype=float
                )
            logger.debug(df)
            table_django = StrategyTable(
                df.to_dict("records"),
                money_per_company=money_per_company,
                count=form.cleaned_data["companies_to_buy"],
            )
    else:
        form = SearchStrategyCalculationForm()
    strategy = Strategy.objects.get(pk=pk)
    strategy_dates = []
    for i in StrategyCalculationResult.objects.filter(strategy=pk).values_list(
        "date", flat=True
    ):
        strategy_dates.append(i.strftime("%d-%m-%Y"))
    return render(
        request,
        "strategies/strategy_result.html",
        {
            "strategy": strategy,
            "strategy_dates": strategy_dates,
            "form": form,
            "table_django": table_django,
        },
    )

