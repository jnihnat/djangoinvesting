from django.contrib import admin
from .models import Strategy, StrategyCalculationResult, StrategyType


admin.site.register(StrategyType)
admin.site.register(Strategy)
admin.site.register(StrategyCalculationResult)