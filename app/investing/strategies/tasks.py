from celery import shared_task, Celery
from celery.signals import setup_logging
from .basestrategy import BaseStrategy
from .strategies import ObjectStrategy
from .models import Strategy


app = Celery("investing")

app.config_from_object("django.conf:settings", namespace="CELERY")


@setup_logging.connect
def config_loggers(*args, **kwargs):
    from logging.config import dictConfig  # noqa
    from django.conf import settings  # noqa

    dictConfig(settings.LOGGING)


@app.task(bind=True)
def calculate_strategy(self, *args, **kwargs):
    strategy_to_calculate = Strategy.objects.get(id=args[0])
    strategy_type = strategy_to_calculate.type.id
    strategy = ObjectStrategy()
    strategy.calculate(
        type=strategy_type, strategy_id=args[0], setup=strategy_to_calculate.setup, date=kwargs.get("date")
    )
