from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Layout, Submit
from django import forms
from django.forms import HiddenInput
from django.utils.translation import gettext as _


class SearchStrategyCalculationForm(forms.Form):
    search_date = forms.DateField(
        label=_("Date of calculation"),
        widget=forms.DateInput(
            attrs={"data-provide": "datepicker", "autocomplete": "off"}
        ),
    )
    money_investest = forms.FloatField(
        label=_("Money to invest"),
        widget=forms.TextInput(attrs={"inputmode": "numeric", "pattern": "[0-9]*"}),
    )
    leverage = forms.FloatField(label=_("Leverage"), widget=forms.TextInput())
    companies_to_buy = forms.IntegerField(
        label=_("Number of companies to buy"), widget=forms.TextInput(),
    )

    def __init__(self, *args, **kwargs):
        super(SearchStrategyCalculationForm, self).__init__(*args, **kwargs)
        self.fields["search_date"].readonly = True
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Div(
                            HTML(_("Strategy calculation")),
                            css_class="app-fx app-left",
                        ),
                        css_class="card-header",
                    ),
                    Div(
                        Div(
                            Div("search_date", css_class="col-sm-3"),
                            Div("money_investest", css_class="col-sm-3"),
                            Div("leverage", css_class="col-sm-3"),
                            Div("companies_to_buy", css_class="col-sm-3"),
                            css_class="row",
                        ),
                    ),
                    Div(FormActions(Submit("save", "Search",),), css_class="mt-3",),
                    css_class="",
                ),
                css_class="",
            ),
        )

