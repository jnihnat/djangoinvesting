from .basestrategy import BaseStrategy
import pandas as pd


class SmoUpgradedStrategy(BaseStrategy):
    def __init__(self, strategy_id, *args, **kwargs):
        super(SmoUpgradedStrategy, self).__init__(strategy_id, *args, **kwargs)
        self.roc_list = kwargs.get("roc_list", [10, 20, 60, 120])
        self.ranking_span = kwargs.get("ranking_span", 60)
        self.tickers_ma_window = kwargs.get("tickers_ma_window", 200)

    def calculation(self):
        ranking = pd.DataFrame(index=self.stock_data.index.copy())
        for i in self.roc_list:
            if ranking.empty:
                ranking = self.calc_roc(i)
            else:
                ranking += self.calc_roc(i)

        ranking = ranking / len(self.roc_list)

        mo_rank = ranking.ewm(span=self.ranking_span).mean()
        mo_score = mo_rank.iloc[-1]
        mo_score = mo_score.sort_values(ascending=False)

        calculated_table = mo_score.to_frame(name="MoScore").reset_index()
        calculated_table = calculated_table.rename(columns={"index": "Ticker"})
        calculated_table["MoScore"] = calculated_table["MoScore"].round(2)
        ma_tickers = (
            self.stock_data["adjusted_close"]
            .rolling(window=self.tickers_ma_window)
            .mean()
        )
        ma_tickers_ok = self.stock_data["adjusted_close"] > ma_tickers

        for (index_label, row_series) in calculated_table.iterrows():
            calculated_table.loc[index_label, "Last_Close"] = self.stock_data["close"][
                calculated_table.loc[index_label, "Ticker"]
            ].iloc[-1]
            calculated_table.loc[index_label, "MA200"] = ma_tickers[
                calculated_table.loc[index_label, "Ticker"]
            ].iloc[-1]
            calculated_table.loc[index_label, "MA200_OK"] = (
                ma_tickers_ok[calculated_table.loc[index_label, "Ticker"]]
                .iloc[-1]
                .astype(bool)
            )
            calculated_table.loc[index_label, "MoScore"] = (
                calculated_table.loc[index_label, "MoScore"]
                if calculated_table.loc[index_label, "MA200_OK"]
                else 0
            )
        calculated_table.sort_values(by=['MoScore'],ascending=False,inplace=True)
        return calculated_table.to_json(orient="index")

    def calculate_strategy(self):
        self.calculated_context = self.kontext_filt()
        self.calculated_result = self.calculation()
        self.save_calculation()
        return 200


class SmoFinancnikStrategy(BaseStrategy):
    def calculate_strategy():
        pass


class ObjectStrategy:
    def calculate(self, type, strategy_id, setup, date=None):
        calculation = factory.get_strategy(type, strategy_id, setup, date)
        calculation.calculate_strategy()
        return "OK"


class StrategyFactory:
    def __init__(self):
        self._creators = {}

    def register_strategy(self, type, creator):
        self._creators[type] = creator

    def get_strategy(self, type, strategy_id, setup, date):
        creator = self._creators.get(type)
        if not creator:
            raise ValueError(type)
        return creator(strategy_id, date, **setup)


factory = StrategyFactory()
factory.register_strategy(1, SmoUpgradedStrategy)
factory.register_strategy(2, SmoFinancnikStrategy)
