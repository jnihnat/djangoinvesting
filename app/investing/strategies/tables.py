import django_tables2 as tables
import itertools
import logging

logger = logging.getLogger(__name__)


class StrategyTable(tables.Table):
    row_number = tables.Column(empty_values=())
    Ticker = tables.Column()
    MoScore = tables.Column()
    Last_Close = tables.Column()
    MA200 = tables.Column()
    MA200_OK = tables.Column()
    number_to_buy = tables.Column(empty_values=())

    def __init__(self, *args, money_per_company=None, count=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count(1)
        self.counter2 = itertools.count()
        self.orderable = False
        if money_per_company and count:
            self.money_per_company = money_per_company
            self.count = count
        else:
            self.exclude = "number_to_buy"

    def render_row_number(self):
        return "%d" % next(self.counter)

    def render_number_to_buy(self, value, record):
        if next(self.counter2) < self.count:
            value_num = round(self.money_per_company / float(record["Last_Close"]))
        else:
            value_num = 0
        return "%d" % value_num

    def render_MA200(self, value):
        return f"{value:.2f}"

    def render_MA200_OK(self, value):
        if value == True:
            return f"OK"
        else:
            return f"NOK"

    def render_Last_Close(self, value):
        return f"{float(value):.2f}"

    def render_MoScore(self, value):
        return f"{float(value):.2f}"
