from .models import Strategy, StrategyCalculationResult
from ..coreData.models import StockHistoricalData, Stock, IndicesConstituent
from ..coreData.scraper import fetch_historical_stock_data
from ..coreData.customExceptions import DateNotInData
import pandas as pd
import talib
from datetime import datetime
import logging
import json

logger = logging.getLogger(__name__)


class BaseStrategy:
    def __init__(self, strategy_id,date_str, *args, **kwargs):
        self.kontext_filt_tickers = kwargs.get("kontext_filt_tickers", ["QQQ", "SPY"])

        self.kontext_filt_window = kwargs.get("kontext_filt_window", 200)

        self.kontext_filt_days = kwargs.get("kontext_filt_days", 1)

        self.strategy = Strategy.objects.get(id=strategy_id)

        indice_dates = IndicesConstituent.objects.filter(
                        indice_ticker=kwargs.get("strategy_tickers")
                    ).values_list("data", flat=True)[0].keys()

        try:
            if date_str == None:
                self.date_str = max(
                    indice_dates
                )
            else:
                self.date_str = date_str
            self.strategy_tickers = IndicesConstituent.objects.get(
                indice_ticker=kwargs.get("strategy_tickers"), data__has_key = self.date_str).data[self.date_str]["tickers"]
        except IndicesConstituent.DoesNotExist:
            dates_list = [datetime.strptime(date, "%Y-%m-%d").date() for date in indice_dates]
            date = datetime.strptime(date_str, "%Y-%m-%d").date()
            nearest_date = min([i for i in dates_list if i <= date], key=lambda x: abs(x - date))
            nearest_date_str = nearest_date.strftime("%Y-%m-%d")
            self.strategy_tickers = IndicesConstituent.objects.get(
                indice_ticker=kwargs.get("strategy_tickers")).data[nearest_date_str]["tickers"]
            logger.debug(f"Using indice constituents from date {nearest_date_str}")
        else:
            logger.debug(f"Using indice constituents from date {self.date_str}")

        self.stock_data = self.load_stock_data(self.strategy_tickers)
        if date_str:
            try:
                self.stock_data.loc[date_str]
            except Exception:
                raise DateNotInData(date_str)
            else:
                self.stock_data=self.stock_data.loc[:date_str]

        self.date = datetime.strptime(self.stock_data.last_valid_index(), "%Y-%m-%d")

    def load_stock_data(self, tickers):
        stock_data = pd.DataFrame()
        for stock_ticker in tickers:
            try:
                stock = Stock.objects.get(ticker=stock_ticker)
                stock_json_data = StockHistoricalData.objects.get(
                    stock=stock
                ).data
            except Stock.DoesNotExist:
                logger.debug(f"No stock data for {stock_ticker} in database, skipping to next ticker")
            except StockHistoricalData.DoesNotExist:
                logger.debug(f"No stock historical data for {stock_ticker} in database, skipping to next ticker")
            else:
                stock_json_data = StockHistoricalData.objects.filter(
                    stock=stock
                ).values_list("data", flat=True)[0]
                logger.debug(stock)
                data_pom = pd.DataFrame.from_dict(
                    stock_json_data, orient="index", dtype=float
                )
                data_pom.columns = [
                    "open",
                    "high",
                    "low",
                    "close",
                    "volume",
                    "adjusted_close",
                ]
                if stock_data.empty:
                    data_pom["ticker"] = stock_ticker
                    data_pom.index = data_pom.index.rename("date")
                    stock_data = data_pom
                else:
                    data_pom["ticker"] = stock_ticker
                    data_pom.index = data_pom.index.rename("date")
                    stock_data = pd.concat([stock_data, data_pom], axis=0, join='outer')
        stock_data = stock_data.pivot(
            columns="ticker",
            values=[
                "open",
                    "high",
                    "low",
                    "close",
                    "volume",
                    "adjusted_close",
            ],
        )
        stock_data = stock_data.fillna(method="backfill")
        stock_data = stock_data.fillna(method="ffill")
        stock_data = stock_data.fillna(value=0)
        return stock_data

    def kontext_filt(self):
        data_kontext = pd.DataFrame()
        data_kontext = self.load_stock_data(self.kontext_filt_tickers)

        data_kontext = data_kontext["adjusted_close"]

        sma_kontext = data_kontext.rolling(window=self.kontext_filt_window).mean()
        sma_check = list()
        i = 1
        while i < self.kontext_filt_days + 1:
            for column in data_kontext:
                sma_check.append(
                    (float(sma_kontext[column][-i]) < float(data_kontext[column][-i]))
                )
            i += 1

        return all(sma_check)

    def calc_roc(self, days):
        rocs = pd.DataFrame(index=self.stock_data.index.copy())
        data_pom = self.stock_data["adjusted_close"]
        for i in list(data_pom):
            rocs[i] = talib.ROC(data_pom[i], days)
        return rocs

    def calculation(self):
        pass

    def save_calculation(self):
        StrategyCalculationResult.objects.update_or_create(
                strategy=self.strategy,
                date=self.date,
                defaults={
                    "strategy": self.strategy,
                    "date": self.date,
                    "kontext_filter_result": self.calculated_context,
                    "filter_result": json.loads(self.calculated_result),
                },
            )