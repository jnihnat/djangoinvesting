from django.urls import path

from . import views

app_name = "investing.strategies"

urlpatterns = [
    path("", views.index, name="index"),
    path("nasdaq-monthly/<int:pk>", views.nasdaq_monthly, name="nasdaq-monthly"),
]
