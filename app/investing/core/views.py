from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django_registration.backends.one_step.views import RegistrationView
from investing.core.forms import AuthUserLoginForm, AuthUserRegistrationForm

class UserLoginView(LoginView):
    authentication_form = AuthUserLoginForm


class UserRegistrationView(RegistrationView):
    form_class = AuthUserRegistrationForm
    success_url = reverse_lazy("strategies:index")
