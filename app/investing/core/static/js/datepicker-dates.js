;
var availableDates = document.getElementById('dates').textContent.replace(/[\"|\[|\]|' ']/g, '').split(',');
console.log("avaiable dates: " + availableDates);

$('.dateinput').datepicker({
    language: "cs",
    autoclose: true,
    beforeShowDay: function (date) {
        //dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        dmy = date.toLocaleDateString("es-CL");
        console.log("date > " + dmy);
        if ($.inArray(dmy, availableDates) != -1) {
            return {
                "enabled": true,
                "classes": "date-enabled",
            }
        } else {
            return false;
        }
    }
});