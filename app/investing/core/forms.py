from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Layout
from django.contrib.auth.forms import AuthenticationForm
from django_registration.forms import RegistrationForm

class AuthUserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(AuthUserLoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

class AuthUserRegistrationForm(RegistrationForm):
    def __init__(self, *args, **kwargs):
        super(AuthUserRegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False