from .base import *

DEBUG = True

ALLOWED_HOSTS = ["investujemsam.test", "investujemsam","localhost"]


STATICFILES_DIRS = [
        BASE_DIR / "core" / "static",
        BASE_DIR / "core" / "static" / "js",
        BASE_DIR / "core" / "static" / "css",
        BASE_DIR / "core" / "static" / "scss",
        BASE_DIR / "core" / "static" / "loga",
        BASE_DIR / "core" / "static" / "img",
    ]

COMPRESS_ROOT = "/var/www/app/investing/core/static"